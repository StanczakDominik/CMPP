#!/usr/bin/env python
# coding: utf-8

import sklearn.datasets
import matplotlib.pyplot as plt
import numpy as np

digits = sklearn.datasets.load_digits()['images']
bw_digits = (digits >= 8)
data = bw_digits.astype(int)

def plot_all_data(plotted_data = data, *args, **kwargs):
    fig, axes = plt.subplots(1, len(plotted_data), *args, **kwargs)
    for ax, image in zip(axes, plotted_data):
        ax.imshow(image)
    plt.show()

def make_W(data):
    W_list = []
    for D in data:
        x = D.flatten() * 2 - 1
        Wn = np.outer(x, x)
        W_list.append(Wn)
    W = np.dstack(W_list).sum(axis=-1) / data.shape[0] - np.eye(data[0].size)
    return W


def act(W, image):
    x = image.flatten() * 2 - 1
    result = np.sign(W @ x).astype(int)
    result_rescaled = (result.reshape(image.shape) + 1) // 2
    return result_rescaled





def run_simple(data, W, steps = 100, plot=False):
    results = []
    for D in data:
        D_working = D.copy()
        if plot:
            fig, axes = plt.subplots(1, 1+steps)
            axes[0].imshow(D)
        for n in range(steps):
            D_working = act(W, D_working)
            if plot:
                axes[1+n].imshow(D_working)
        result = (D_working == D).all() | (D_working == ~D).all()
        if plot:
            print(f"Equality: {result}")
            plt.show()
        results.append(result)
    return np.array(results)
# run_simple(data, W, plot=False)


# In[89]:


def recognize(D_working, data):
    product = np.prod(D_working == data, axis=(1, 2))
    if (product == 0).all():
        product = np.prod((1-D_working) == data, axis=(1, 2))
    return data[np.argmax(product)]


# In[92]:


def run_perturbations(n_flipped, data=data, seed=0, steps=11):
    np.random.seed(seed)
    for D in data:
        D_working = D.copy()
        for _ in range(n_flipped):
            x = np.random.randint(0, D_working.shape[0])
            y = np.random.randint(0, D_working.shape[1])
            D_working[x, y] = 1 - D_working[x, y]

        fig, axes = plt.subplots(2, (3+steps)//2)
        assert steps % 2 == 1
        axes = axes.flatten()
        axes[0].imshow(D)
        axes[0].set_title("Original image")
        axes[1].imshow(D_working)
        axes[1].set_title("Perturbed image")
        for n in range(steps):
            D_working = act(W, D_working)
            axes[2+n].imshow(D_working)
            axes[2+n].set_title(f"Step {n+1}")
#         recognized =  data[np.argmax(np.prod(D_working == data, axis=(1, 2)))]
        recognized = recognize(D_working, data)
        axes[-1].imshow(recognized)
        axes[-1].set_title("Recognized image")
        plt.tight_layout()
        plt.show()
        print(f"Equality: {(D_working == D).all()}")
        print(f"Recognized: {(recognized == D).all()}")

def overlap(pic1, pic2):
    pic1 = 2 * pic1 - 1
    pic2 = 2 * pic2 - 1
    assert pic1.shape == pic2.shape
    return (pic1 * pic2).mean()

def get_overlaps(data, n_flipped = 1, seed = 0, steps=11, plot=False):
    np.random.seed(seed)
    W = make_W(data)
    overlaps = []
    for D in data:
        D_working = D.copy()
        for _ in range(n_flipped):
            x = np.random.randint(0, D_working.shape[0])
            y = np.random.randint(0, D_working.shape[1])
            D_working[x, y] = 1 - D_working[x, y]

        for n in range(steps):
            D_temporary = act(W, D_working)
            if (D_temporary == D_working).all():
                # print(f"convergence after {n} steps")
                break
            else:
                D_working = D_temporary
        if plot:
            fig, axes = plt.subplots(1, 3)
            axes[0].imshow(D)
            axes[1].imshow(D_working)
            axes[2].imshow(D_working - D)
        overlaps.append(overlap(D, D_working))
    return overlaps
