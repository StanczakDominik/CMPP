import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm as tqdm
import multiprocessing
from functools import partial
import random
import pandas as pd

plt.rcParams['figure.figsize'] = [11, 6]  # for larger pictures

np.random.seed(0)

def energy(a, B, J=1):
    interactions = np.roll(a, 1, 0) + np.roll(a, 1, 1) + np.roll(a, -1, 0) + np.roll(a, -1, 1)
    current_energy = (-J * interactions + B) * a
    return current_energy

def half_iteration(a, B, mask, J = 1, beta = 1):
    deltaE = -2 * energy(a, B, J)
    boltzmann = np.exp(-beta * deltaE) * mask
    flip_these = np.random.random(a.shape) < boltzmann
    a[flip_these] *= -1

# make a checkerboard pattern to preserve causality, updating spins on alternating diagonals in each iteration
def mask(a):
    a_mask = np.ones_like(a)
    a_mask[::2, ::2] = 0
    a_mask[1::2, 1::2] = 0
    return a_mask

def full_iteration(a, B, mask, J = 1, beta = 1):
        # this now becomes a VERY thin wrapper!
    for i in range(2):
        if random.randint(0, 1):
            half_iteration(a, B, mask, J, beta)
        else:
            half_iteration(a, B, 1-mask, J, beta)


def full_correlated_iteration(a, B, mask, J = 1, beta = 1):
    half_iteration(a, B, mask, J, beta)
    half_iteration(a, B, 1-mask, J, beta)
            
def magnetization(a):
#     interactions = np.roll(a, 1, 0) + np.roll(a, 1, 1) + np.roll(a, -1, 0) + np.roll(a, -1, 1)
#     mod_a = np.abs(a)
    return a.mean()

def internal_energy(a, B, J = 1):
    current_energy = energy(a, B, J)
    return current_energy.mean()

def actually_run_single_simulation(beta, A, size, a_mask, B, initial_iterations, iterations, iteration_algo=full_iteration):
    if isinstance(A, float) or isinstance(A, int):
        a = A*np.ones(size)
    elif A == "random":
        a = np.random.randint(0, 2, size) - 1
    elif isinstance(A, np.ndarray):
        a = A.copy()
    else:
        raise(ValueError)

    M = []
    E = []
    for i in range(initial_iterations):
        iteration_algo(a, B, a_mask, beta=beta)

    for i in range(iterations):
        E.append(internal_energy(a, B))
        M.append(a.mean())
        iteration_algo(a, B, a_mask, beta=beta)
    return E, M, a

Onsager_critical_temperature = 2 / np.log(1 + 2**0.5)  # a theoretical value
def run_single_simulation(T, iterations, size, A, B, k_b, a_mask,
                          initial_iterations, n_tries):
    beta = 1 / (k_b * T)
    
    energies = []
    energy_variances = []
    magnetizations = []
    susceptibilities = []
    
    for n in range(n_tries):
        E, M, _ = actually_run_single_simulation(beta, A, size, a_mask, B)
        
        energies.append(np.mean(E))
        energy_variances.append(np.var(E) * beta / T * size[0]**2)
        magnetizations.append(np.mean(np.abs(M)))
        susceptibilities.append(np.var(Mnoabs) * beta * size[0]**2)
        
    return (np.mean(energies), np.std(energies)), (np.mean(energy_variances), np.std(energy_variances)), (np.mean(magnetizations), np.std(magnetizations)), (np.mean(susceptibilities), np.std(susceptibilities))
    

def simulation(iterations=5000,
               size=(64,64),
               A = 1,
               B = None,
               initial_iterations=500,
               T_range = np.linspace(1e-6, 8),
               k_b=1,
               plotting=True,
               show_samples=False,
               n_tries=10,
               n_procs = None):
    energies = []
    magnetizations = []
    As = []
    a_mask = np.ones(size)
    a_mask[::2, ::2] = 0
    a_mask[1::2, 1::2] = 0
    if B is None:
        B = np.zeros(size)
    
    parallelizable_function = partial(run_single_simulation, iterations=iterations, initial_iterations=initial_iterations, size=size, A = A, B=B, k_b=k_b, a_mask=a_mask, n_tries=n_tries)
    
    if n_procs is not None:
        with multiprocessing.Pool(n_procs) as p:
            results = list(tqdm(p.imap(parallelizable_function, T_range), total=len(T_range)))
    else:
        results = list(tqdm(map(parallelizable_function, T_range), total=len(T_range)))
    
    energies, energy_variances, magnetizations, susceptibilities = zip(*results)
    E, dE = np.vstack(energies).T
    stdE, dstdE = np.vstack(energy_variances).T
    M, dM = np.vstack(magnetizations).T
    susceptibilities, dsusceptibilities = np.vstack(susceptibilities).T

    maximum_index = np.argmax(stdE)
    our_critical_temperature = T_range[maximum_index]
    df = pd.DataFrame(data={'Temperature':T_range, 'E':E, 'dE':dE, 'M':M, 'dM':dM, 'Cv':stdE, 'dCv':dstdE, 'susceptibility': susceptibilities, 'dsusceptibility':dsusceptibilities})
    df.to_csv("ising_wyniki.csv")
    if plotting:
        plot(df)

def plot(df, lineformat="o--", show=False):
    fig, axes = plt.subplots(2, 2, sharex=True)
    axes[0,0].errorbar(df.Temperature, df.E, df.dE, fmt=lineformat)
    axes[0,0].set_ylabel("Average energy per spin")
    axes[0,0].set_xlim(df.Temperature.min(), df.Temperature.max());
    axes[0,0].axvline(Onsager_critical_temperature, color='black')

    axes[0,1].errorbar(df.Temperature, df.M, df.dM, fmt=lineformat)
    axes[0,1].set_ylabel("Average magnetization")
    axes[0,1].set_xlim(df.Temperature.min(), df.Temperature.max());
    axes[0,1].axvline(Onsager_critical_temperature, color='black')

    axes[1,0].errorbar(df.Temperature, df.Cv, df.dCv, fmt=lineformat)
    axes[1,0].set_xlabel(r"Temperature [in units where $k_B = 1$]")
    axes[1,0].set_ylabel("Heat capacity per spin")
    axes[1,0].set_xlim(df.Temperature.min(), df.Temperature.max());
    axes[1,0].axvline(Onsager_critical_temperature, color='black')
    
    axes[1,1].errorbar(df.Temperature, df.susceptibility, df.dsusceptibility, fmt=lineformat)
    axes[1,1].set_xlabel(r"Temperature [in units where $k_B = 1$]")
    axes[1,1].set_ylabel("Magnetic susceptibility")
    axes[1,1].set_xlim(df.Temperature.min(), df.Temperature.max());
    axes[1,1].axvline(Onsager_critical_temperature, color='black')
    plt.tight_layout()
    fig.savefig("ising_wyniki.pdf")
    if show:
        plt.show()
    else:
        plt.close()

#simulation(iterations=500, initial_iterations=500, size=(6,6), T_range=np.array([5, 4, 3, 2, 1]), B=0)
# simulation( B=0, n_procs=4, T_range = np.linspace(1, 3.5, 100))
# plot(pd.read_csv("ising_wyniki.csv"), show=True)
