import numpy as np
import matplotlib.pyplot as plt
import seaborn
import tqdm
from collections import deque
import random
import pandas as pd

plt.rcParams['figure.figsize'] = [11, 6]  # for larger pictures

np.random.seed(0)

Lbig = 600
size = (Lbig, Lbig)

def neighbour(i):
    ix, iy = i
    return [ ( ix, (iy+1)%Lbig ), ( ix, (iy-1)%Lbig ),
            ( (ix+1)%Lbig, iy ), ( (ix-1)%Lbig, iy ) ]


def field(s, h, H):
    interactions = np.roll(s, 1, 0) + np.roll(s, 1, 1) + np.roll(s, -1, 0) + np.roll(s, -1, 1)
    current_field = (J * interactions + h + H)
    return current_field



def update(i, s, heff):
    s[i] = 1
    for j in neighbour(i):
        heff[j] += 2.0

def plot(i, s, heff):
    fig, (spin_ax, h_ax) = plt.subplots(1, 2)
    plt.suptitle(f"Iteration {i}")
    spin_im = spin_ax.imshow(s, interpolation='none')
    h_im = h_ax.imshow(heff, interpolation='none')
    plt.tight_layout()
    plt.show()

for R in tqdm.tqdm([0.9, 1.4, 2.1]):
    first_avalanches = []
    s = -np.ones(size, dtype=int)
    J = 1
    h = np.random.normal(loc=0, scale=R, size=size)
    heff = -4 + h 
    i = 0
    queue = deque()
    avalanche_numbers = np.nan * np.ones(size)
    H_list = []
    M_list = []
    with tqdm.tqdm(total=s.size) as pbar:
        while (s == -1).any():
            itrig = np.unravel_index(np.argmax( heff + (s+1)*(-100) ),heff.shape)
            H = -heff[itrig]
            H_list.append(H)
            queue.append(itrig)
            updated = 0
            while queue:
                popped_spin_index = queue.popleft()
                if s[popped_spin_index] == -1:
                    avalanche_numbers[popped_spin_index] = i
                    update(popped_spin_index, s, heff)
                    updated += 1
                    for neighbor in neighbour(popped_spin_index):
                        if heff[neighbor] + H > 0:
                            queue.append(neighbor)
            i+= 1
            pbar.update(updated)
            M_list.append(s.mean())
    fig, ((HM_ax, av_ax), (avsize_ax, newax)) = plt.subplots(2, 2)
    plt.suptitle(f"R = {R}")
    avalanche_im = av_ax.imshow(avalanche_numbers,interpolation='none',cmap="plasma")
    plt.colorbar(avalanche_im)
    HM_ax.plot(M_list, H_list, ',')
    HM_ax.plot(M_list, H_list, '-', alpha=0.5)
    HM_ax.set_xlim(-1, 1)
    HM_ax.set_ylim(-3, 3)
    avalanche_nums, avalanche_num_counts = np.unique(np.ravel(avalanche_numbers), return_counts=True)
    avsize_ax.plot(avalanche_nums, avalanche_num_counts)
    avsize_ax.set_ylabel("Avalanche size")
    avsize_ax.set_xlabel("Avalanche number (~time)")
    seaborn.distplot(np.log(avalanche_num_counts), ax=newax, kde=False)
    newax.set_xlabel("log[Avalanche size]")
    # newax.set_xscale('log')
    # newax.set_yscale('log')
plt.show()
