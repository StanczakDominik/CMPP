# coding: utf-8
get_ipython().run_line_magic('run', 'ising_parallel_correlation.py')
size = (64, 64)
a_mask = np.ones(size)
a_mask[::2, ::2] = 0
a_mask[1::2, 1::2] = 0
 
get_ipython().run_line_magic('pinfo', 'actually_run_single_simulation')
beta = 1
A = np.random.random(size)
B = 0
actually_run_single_simulation(beta, A, size, a_mask, B)
get_ipython().run_line_magic('run', 'ising_parallel_correlation.py')
actually_run_single_simulation(beta, A, size, a_mask, B, 3000, 3000)
a = _[-1]
a
A = np.random.randint(0, 2, size)
A
A = np.random.randint(0, 2, size) * 2 - 1
actually_run_single_simulation(beta, A, size, a_mask, B, 3000, 3000)
A.mean()
a
a = actually_run_single_simulation(beta, A, size, a_mask, B, 3000, 3000)[-1]
a
a.mean()
plt
plt.imshow(a)
plt.show()
get_ipython().run_line_magic('pinfo', 'np.convolve')
get_ipython().run_line_magic('pinfo', 'np.convolve')
np.convolve(a.ravel(), a.ravel())
plt.plot(_)
plt.show()
get_ipython().run_line_magic('pinfo', 'np.convolve')
from scipy.signal import fftconvolve
fftconvolve(a.ravel(), a.ravel())
plt.plot(_)
plt.show()
a = actually_run_single_simulation(beta, A, size, a_mask, B, 3000, 3000)[-1].ravel()
plt.plot(a)
plt.show()
get_ipython().run_line_magic('pinfo', 'np.roll')
multiplications = np.vstack([a * np.roll(a, i) for i in range(size[0])])
plt.imshow(multiplications)
plt.show(0)
size
a.size
a.shape
multiplications = np.vstack([a * np.roll(a, i) for i in range(a.size)])
plt.imshow(multiplications)
plt.show()
correlations = np.vstack([a * np.roll(a, i) for i in range(a.size)]).mean(axis=1)
correlations
plt.plot(correlations)
plt.show()
a = actually_run_single_simulation(0.1, A, size, a_mask, B, 3000, 3000)[-1].ravel()
correlations = np.vstack([a * np.roll(a, i) for i in range(a.size)]).mean(axis=1)
plt.plot(correlations)
plt.show()
correlations = np.vstack([a * np.roll(a, i) for i in range(1, a.size)]).mean(axis=1)
plt.plot(correlations)
plt.show()
get_ipython().run_line_magic('run', 'ising_parallel_correlation.py')
a = actually_run_single_simulation(0.1, A, size, a_mask, B, 3000, 3000, iteration_algo=full_correlated_iteration)[-1].ravel()
correlations = np.vstack([a * np.roll(a, i) for i in range(1, a.size)]).mean(axis=1)
plt.show()
plt.plot(correlations)
plt.show()
plt.plot(correlations)
a_good = actually_run_single_simulation(0.1, A, size, a_mask, B, 3000, 3000)[-1].ravel()
correlations = lambda a: np.vstack([a * np.roll(a, i) for i in range(1, a.size)]).mean(axis=1)
plt.plot(correlations(a_good))
plt.show()
import seaborn
seaborn.distplot(correlations(a))
seaborn.distplot(correlations(a_good))
plt.show()
a_good = actually_run_single_simulation(0.1, A, (128, 128), a_mask, B, 30000, 30000)[-1].ravel()
correlations = lambda a: np.array([np.mean(a * np.roll(a, i)) - np.mean(a) * np.mean(np.roll(a, i)) for i in range(1, a.size)])
correlations(a_good)
plt.plot(_)
plt.show()
plt.plot(correlations(a_good))
plt.plot(correlations(a))
plt.show()
a_good = actually_run_single_simulation(0.1, A, (128, 128), a_mask, B, 30000, 30000)[-1].ravel()
a = actually_run_single_simulation(0.1, A, (128, 128), a_mask, B, 30000, 30000, iteration_algo=full_correlated_iteration)[-1].ravel()
seaborn.distplot(correlations(a))
seaborn.distplot(correlations(a_good))
plt.show()
a_good = actually_run_single_simulation(1, A, (128, 128), a_mask, B, 30000, 30000)[-1].ravel()
a = actually_run_single_simulation(1, A, (128, 128), a_mask, B, 30000, 30000, iteration_algo=full_correlated_iteration)[-1].ravel()
seaborn.distplot(correlations(a))
seaborn.distplot(correlations(a_good))
plt.show()
a.reshape(size)
plt.imshow(a.reshape(size))
plt.show()
plt.imshow(a_good.reshape(size))
plt.show()
a = actually_run_single_simulation(0.1, A, (128, 128), a_mask, B, 30000, 30000, iteration_algo=full_correlated_iteration)[-1].ravel()
a_good = actually_run_single_simulation(0.1, A, (128, 128), a_mask, B, 30000, 30000)[-1].ravel()
